import React, { Component } from 'react';
import { Doughnut } from 'react-chartjs-2';

class GraficoComponent extends Component {
  render() {
    return (
      <div>
        <Doughnut data={...} />
      </div>
    );
  }
}

export default GraficoComponent;