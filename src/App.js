import React, { Component } from 'react';
import './App.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Line } from 'react-chartjs-2';
import { registerLocale } from 'react-datepicker';
import es from 'date-fns/locale/es';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { Table } from 'react-bootstrap';
import { format } from 'date-fns';
import subDays from 'date-fns/subDays';
import addDays from 'date-fns/addDays';
registerLocale('es', es);

class App extends Component {
  constructor() {
    super();
    this.state = {
      fecha_i: new Date(),
      fecha_f: new Date(),
      dolares: [],
      valores: [],
      labels: [],
      promedio: 0,
      valorMin: 0,
      valorMax: 0,
    };
  }

  async componentDidMount() {
    this.getDolars();
  }

  getDolars = () => {
    const fechaDesde =
      format(this.state.fecha_i, 'yyyy') +
      '/' +
      format(this.state.fecha_i, 'MM') +
      '/dias_i/' +
      format(this.state.fecha_i, 'dd');

    const fechaHasta =
      format(this.state.fecha_f, 'yyyy') +
      '/' +
      format(this.state.fecha_f, 'MM') +
      '/dias_f/' +
      format(this.state.fecha_f, 'dd');

    const apiKEY = '9c84db4d447c80c74961a72245371245cb7ac15f';
    const apiURL =
      'https://api.sbif.cl/api-sbifv3/recursos_api/dolar/periodo/' +
      fechaDesde +
      '/' +
      fechaHasta +
      '?apikey=' +
      apiKEY +
      '&formato=json';

    fetch(apiURL)
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        this.setState(
          { dolares: response.Dolares },
          this.armaValores(response.Dolares),
          this.armaLabels(response.Dolares)
        );
      });
  };

  armaValores(data) {
    var valores = [];
    var valorMin = 0;
    var valorMax = 0;
    data.map((dolar) => {
      let valor = parseFloat(dolar.Valor);
      valores.push(valor);

      if (valorMax !== 0) {
        if (valor > valorMax) {
          valorMax = valor;
        }
      } else {
        valorMax = valor;
      }

      if (valorMin !== 0) {
        if (valor < valorMin) {
          valorMin = valor;
        }
      } else {
        valorMin = valor;
      }
    });
    let sum = valores.reduce((previous, current) => (current += previous));
    let prom = sum / valores.length;

    this.setState({ valores: valores, promedio: prom, valorMin: valorMin, valorMax: valorMax });
  }

  armaLabels(data) {
    var labels = [];
    data.map((dolar) => {
      labels.push(dolar.Fecha);
    });
    this.setState({ labels: labels });
  }

  onSelectFechaInicio = () => {
    console.log('onSelectFechaInicio');
  };

  onChangeFechaInicio = (fecha) => {
    this.setState({ fecha_i: fecha }, () => {
      this.getDolars();
    });
  };

  onSelectFechaFinal = () => {
    console.log('onSelectFechaFinal');
  };

  onChangeFechaFinal = (fecha) => {
    this.setState({ fecha_f: fecha }, () => {
      this.getDolars();
    });
  };

  render() {
    const listItems = this.state.dolares.map((dolar, index) => (
      <tr key={index}>
        <td>{index}</td>
        <td>{dolar.Valor}</td>
        <td>{dolar.Fecha}</td>
      </tr>
    ));

    const data = {
      labels: this.state.labels,
      datasets: [
        {
          label: 'Fechas',
          data: this.state.valores,
          fill: true,
          backgroundColor: 'rgba(75,192,192,0.2)',
          borderColor: 'rgba(75,192,192,1)',
        },
      ],
    };

    return (
      <div className="container mt-5">
        <div className="row">
          <div className="col-3">
            <p>Inicio:</p>
            <DatePicker
              selected={this.state.fecha_i}
              onSelect={this.onSelectFechaInicio}
              onChange={this.onChangeFechaInicio}
              locale="es"
              minDate={subDays(new Date(), 30)}
            />
          </div>

          <div className="col-3">
            <p>Final:</p>
            <DatePicker
              selected={this.state.fecha_f}
              onSelect={this.onSelectFechaFinal}
              onChange={this.onChangeFechaFinal}
              locale="es"
              minDate={new Date()}
              maxDate={addDays(new Date(), 30)}
            />
          </div>
        </div>

        <div className="row">
          <div className="col mt-3">
            <p>
              <strong>Promedio: </strong> {'$ ' + this.state.promedio.toFixed(2).replace(/[^\d]+/g, ',') + ' '}
              <strong>Min: </strong> {'$ ' + this.state.valorMin.toFixed(2).replace(/[^\d]+/g, ',') + ' '}
              <strong>Max: </strong> {'$ ' + this.state.valorMax.toFixed(2).replace(/[^\d]+/g, ',') + ' '}
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <Line data={data} />
          </div>
          <div className="col col-md-3 mt-4">
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Valor</th>
                  <th>Fecha</th>
                </tr>
              </thead>
              <tbody>{listItems}</tbody>
            </Table>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
